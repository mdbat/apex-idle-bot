# Apex Idle bot
This program is designed for farming XP on Apex Legends by continuously queuing 
for matches and moving around in game to avoid kicks due to inactivity.  
The bot accounts for any UI prompt, error message, etc. and can also select a
specific character on the character selection screen.

## Setup
* Install Python 3 from https://www.python.org/downloads/. Check the option
"Add to PATH" to be able to launch python from the commandline.
* Install the packages required by the program using PIP with the command
"pip install ..." (or "pip3 install..."). The required packages are:
    * pypiwin32 : for access to the Windows API (windowing, input, ...).
    * pillow : used to take screenshots of the desktop.
    * opencv-python : used for image analysis.
* Download this program (duh !).

## How to run
* Start Apex legends in 720p resolution and windowed mode, and simply run the program 
using the command "python bot.py" (or "python3 bot.py"). The bot should start its
keypresses 5 seconds after launch.  
* Launch options: You can specify launch options to the bot. They are either in the
format 'name=value' (e.g. oneplusone=two) or simply 'name' for options that are just
an on-off switch.
Available launch options are:
    * 'hero=XXX'. If you want the bot to select a character for you on the selection screen, add the
    name of the character you desire in all lowercase as a parameter when starting the
    bot, for example "python bot.py hero=gibraltar".
    * 'mode=XXX'. If you want the bot to select a particular mode, add the name of the mode you desire
    as a parameter. Can either be 'ranked', 'trios', 'duos' or 'mixtape'
    * 'leave-on-respawn-expired'. If you want to leave as soon as you cannot be respawned by teammates,
    add this option. Can be useful especially in Ranked mode in a derank scenario.