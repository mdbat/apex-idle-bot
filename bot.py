import random

import keyinput, time, win32gui, win32api, win32con, win32process, cv2, sys, ctypes
from PIL import ImageGrab
import numpy as np
import os, signal, threading
import re
import datetime
import signal


# Exit on interrupt (this is a multithreaded program so do it ourselves)
def interrupt_handler(signum, frame):
    os._exit(1) # Kinda hacky but I couldnt care less about a clean exit


signal.signal(signal.SIGINT, interrupt_handler)


# Position of the apex legends window
window_x = -1
window_y = -1
window_width = -1
window_height = -1

# Windows process related stuff
window_pid = -1
window_path = ""
window_found = False

# Whether this is targetting the steam version of the game,
# known through the use of the "steam" launch options
isSteamVersion = False

# Launch options parsed as arguments in the "optionname" or "optioname=value" format
launch_options = {}


def log(msg: str):
    now = datetime.datetime.now()
    print(f'[{now.hour:{"0"}{2}}:{now.minute:{"0"}{2}}:{now.second:{"0"}{2}}] {msg}')


# Routine to kill a process, used when it gets frozen permanently
def killprocess(pid):
    try:
        os.kill(pid, signal.SIGTERM)
    except:
        print('Failed to kill the process, will try again later.')


# Callback for windows enumeration
def getapexwindow(hwnd, extra):
    if win32gui.GetWindowText(hwnd) != "Apex Legends":
        return

    global window_found
    window_found = True

    global window_pid
    global window_path
    thread_id, window_pid = win32process.GetWindowThreadProcessId(hwnd)

    try:
        handle = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION | win32con.PROCESS_VM_READ, False, window_pid)
    except:
        print('Failed to open process')
        return

    window_path = win32process.GetModuleFileNameEx(handle, 0)

    rect = win32gui.GetClientRect(hwnd)
    window_pos = win32gui.ClientToScreen(hwnd, (0, 0))
    global window_x
    global window_y
    global window_width
    global window_height
    window_x = window_pos[0]
    window_y = window_pos[1]
    window_width = rect[2]
    window_height = rect[3]


# Keys to press to move around
keys = [keyinput.W, keyinput.D, keyinput.S, keyinput.A]

# When moving around, how long in seconds since the last keypress.
last_keypress_timestamp = time.time()

# Start with a 'random' duration of activity 30 seconds
random_duration = 30


# Function to move around ingame
def move_around():
    current_time = time.time()

    global last_keypress_timestamp
    global random_duration

    if (current_time - last_keypress_timestamp) > random_duration:
        rand_key_index = random.randint(0, 3)

        keyinput.holdKey(keys[rand_key_index], 0.1)

        last_keypress_timestamp = time.time()

        # Re-generate a random number between 120 and 180. If the amount of seconds since last keypress is greater,
        # We press a random key for a tenth of a second.
        random_duration = random.uniform(120.0, 180.0)


# Function to press ESC
def press_esc(x, y):
    keyinput.pressKey(keyinput.ESC)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.ESC)


# Function to press space
def press_space(x, y):
    keyinput.pressKey(keyinput.SPACE)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.SPACE)


# Function to kill the game when an engine error pops up
def kill_game_for_error(x, y):
    log('Killing the game after having detected an error')
    killprocess(window_pid)


def kill_game_for_ready_timeout():
    log('Killing the game because we did not ready up for an hour')
    global last_ready_up
    last_ready_up = time.time()
    killprocess(window_pid)


last_ready_up = time.time()


def ready_up(x, y):
    global last_ready_up
    last_ready_up = time.time()
    keyinput.click(x, y)


def hold_ctrl(x, y):
    keyinput.holdKey(keyinput.CTRL, 2.0)


def hold_space(x, y):
    keyinput.holdKey(keyinput.SPACE, 3.0)


def press_e_and_aim_down(x, y):
    keyinput.pressKey(keyinput.E)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.E)

    # Aim down for about three seconds
    for i in range(300):
        # This is window-specific
        keyinput.move_directinput(0, 300)
        time.sleep(0.01)


def launch_at_random_time(x, y):
    duration = random.uniform(0.0, 20.0)

    time.sleep(duration)

    keyinput.pressKey(keyinput.E)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.E)


def wait_for_second_player(x, y):
    log("Waiting for second player...")
    time.sleep(60.0)


def wait_for_third_player(x, y):
    log("Waiting for third player...")
    time.sleep(60.0)


# Find a certain item in an image, returns the estimated position and associated threshold
def find_item(img, template):
    # Apply template Matching
    res = cv2.matchTemplate(img, template, cv2.TM_CCOEFF_NORMED)

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    w, h = template.shape[::-1]

    # Top left of the area
    top_left = max_loc
    # Buttom right of the area
    bottom_right = (top_left[0] + w, top_left[1] + h)

    # Zone to click if we do need
    middle_x = (top_left[0] + bottom_right[0]) / 2
    middle_y = (top_left[1] + bottom_right[1]) / 2

    return {'threshold': max_val, 'x': middle_x, 'y': middle_y, 'raw_result': res}


# Take a screenshot of the window
def screenshot(x, y, width, height):
    image = ImageGrab.grab(bbox=(x, y, x + width, y + height))
    image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
    return image


# The previous image used in the watchdog
old_img = None


# Whether we should kill the game when it appears frozen
kill_if_frozen = True


# Whether we should kill the game if we haven't been readying up for a long time
kill_if_ready_timeout = True


# Watchdog running on another thread, makes sure the Apex process didn't crash or is not stucked
def process_watchdog():
    while True:
        # Check for the process being alive every 45 seconds
        time.sleep(45)

        global window_pid
        global window_path
        global window_found

        # Try to find the window handle
        hwnd = win32gui.FindWindow(None, "Apex Legends")

        # Make sure the Window is still alive
        if hwnd == 0:
            window_found = False
            # Restart the process if the window died
            log('Game is not on, re-starting it...')

            # If this is the Origin version, we can directly use the executable itself
            if not isSteamVersion:
                os.system('"' + window_path + '"')
            # If this is the Steam version, use the steam URL or the game won't be able to connect online
            else:
                os.system("start \"\" steam://rungameid/1172470")

            continue

        # Refresh the window's position
        win32gui.EnumWindows(getapexwindow, None)

        # Put it in foreground
        try:
            win32gui.SetForegroundWindow(hwnd)
        except:
            print('Failed to put the window in foreground')

        global kill_if_frozen
        global old_img

        if kill_if_frozen:
            # Capture the window in color
            color_img = screenshot(window_x, window_y, window_width, window_height)

            # Convert it to grayscale for faster processing
            img = cv2.cvtColor(color_img, cv2.COLOR_BGR2GRAY)

            if old_img is not None:
                # Compare the current image to the old one,
                # if it didn't change within 60 seconds, the process is likely stuck
                result = find_item(img, old_img)

                # We deem the image as being the same if it matches over 99.999%
                if result['threshold'] > 0.99999:
                    log('Process appears to be frozen, killing it for a later restart')
                    # Kill the process if it matches
                    killprocess(window_pid)

            # Save the current image to compare it to the next one being read
            old_img = img


# Templates that we are looking for in the image
exit_confirm_button = cv2.imread("Images/exit_confirm_button.png", cv2.IMREAD_GRAYSCALE)
exit_confirm_button_hovered = cv2.imread("Images/exit_confirm_button_hovered.png", cv2.IMREAD_GRAYSCALE)
leave_button = cv2.imread("Images/leave_button.png", cv2.IMREAD_GRAYSCALE)
leave_button_hovered = cv2.imread("Images/leave_button_hovered.png", cv2.IMREAD_GRAYSCALE)
exit_to_lobby_button = cv2.imread("Images/exit_to_lobby_button.png", cv2.IMREAD_GRAYSCALE)
exit_to_lobby_button_hovered = cv2.imread("Images/exit_to_lobby_button_hovered.png", cv2.IMREAD_GRAYSCALE)
ready_button = cv2.imread("Images/ready_button.png", cv2.IMREAD_GRAYSCALE)
ready_button_hovered = cv2.imread("Images/ready_button_hovered.png", cv2.IMREAD_GRAYSCALE)
requeue_button = cv2.imread("Images/requeue_button.png", cv2.IMREAD_GRAYSCALE)
requeue_button_hovered = cv2.imread("Images/requeue_button_hovered.png", cv2.IMREAD_GRAYSCALE)
stats_continue_button = cv2.imread("Images/stats_continue_button.png", cv2.IMREAD_GRAYSCALE)
squad_eliminated_banner = cv2.imread("Images/squad_eliminated_banner.png", cv2.IMREAD_GRAYSCALE)
solo_eliminated_banner = cv2.imread("Images/solo_eliminated_banner.png", cv2.IMREAD_GRAYSCALE)
match_summary_banner = cv2.imread("Images/match_summary_banner.png", cv2.IMREAD_GRAYSCALE)
champions_banner = cv2.imread("Images/champions_banner.png", cv2.IMREAD_GRAYSCALE)
defeat_banner = cv2.imread("Images/defeat_banner.png", cv2.IMREAD_GRAYSCALE)
victory_banner = cv2.imread("Images/victory_banner.png", cv2.IMREAD_GRAYSCALE)
summary_button = cv2.imread("Images/summary_button.png", cv2.IMREAD_GRAYSCALE)
summary_button_hovered = cv2.imread("Images/summary_button_hovered.png", cv2.IMREAD_GRAYSCALE)
dialog_button = cv2.imread("Images/dialog_button.png", cv2.IMREAD_GRAYSCALE)
error_dialog_button = cv2.imread("Images/error_dialog_button.png", cv2.IMREAD_GRAYSCALE)
error_dialog_button_hovered = cv2.imread("Images/error_dialog_button_hovered.png", cv2.IMREAD_GRAYSCALE)
title_screen_continue_button = cv2.imread("Images/title_screen_continue_button.png", cv2.IMREAD_GRAYSCALE)
title_screen_retry_button = cv2.imread("Images/title_screen_retry_button.png", cv2.IMREAD_GRAYSCALE)
announcement_close_button = cv2.imread("Images/announcement_close_button.png", cv2.IMREAD_GRAYSCALE)
directx_device_hung = cv2.imread("Images/device_hung.png", cv2.IMREAD_GRAYSCALE)
return_to_lobby_button = cv2.imread("Images/return_to_lobby_button.png", cv2.IMREAD_GRAYSCALE)
return_to_lobby_button_hovered = cv2.imread("Images/return_to_lobby_button_hovered.png", cv2.IMREAD_GRAYSCALE)
game_logic_error_message = cv2.imread("Images/game_logic_error_message.png", cv2.IMREAD_GRAYSCALE)
news_button = cv2.imread("Images/news_button.png", cv2.IMREAD_GRAYSCALE)
battle_pass_close_button = cv2.imread("Images/battle_pass_close_button.png", cv2.IMREAD_GRAYSCALE)
battle_pass_close_button_hovered = cv2.imread("Images/battle_pass_close_button_hovered.png", cv2.IMREAD_GRAYSCALE)
dev_error_button = cv2.imread("Images/dev_error_button.png", cv2.IMREAD_GRAYSCALE)


# Optional templates for additional features
open_menu_button = cv2.imread("Images/open_menu_button.png", cv2.IMREAD_GRAYSCALE)
leave_match_can_be_respawned = cv2.imread("Images/leave_match_can_be_respawned.png", cv2.IMREAD_GRAYSCALE)

# Optional templated for the game mode selection
ranked_mode_selected = cv2.imread("Images/ranked_mode_selected.png", cv2.IMREAD_GRAYSCALE)
trios_mode_selected = cv2.imread("Images/trios_mode_selected.png", cv2.IMREAD_GRAYSCALE)
duos_mode_selected = cv2.imread("Images/duos_mode_selected.png", cv2.IMREAD_GRAYSCALE)
firing_range_mode_selected = cv2.imread("Images/firing_range_mode_selected.png", cv2.IMREAD_GRAYSCALE)
training_mode_selected = cv2.imread("Images/training_mode_selected.png", cv2.IMREAD_GRAYSCALE)
tdm_mode_selected = cv2.imread("Images/tdm_mode_selected.png", cv2.IMREAD_GRAYSCALE)
ranked_mode_button = cv2.imread("Images/ranked_mode_button.png", cv2.IMREAD_GRAYSCALE)
trios_mode_button = cv2.imread("Images/trios_mode_button.png", cv2.IMREAD_GRAYSCALE)
duos_mode_button = cv2.imread("Images/duos_mode_button.png", cv2.IMREAD_GRAYSCALE)
tdm_mode_button = cv2.imread("Images/tdm_mode_button.png", cv2.IMREAD_GRAYSCALE)

# Optional buttons for when fill/no-fill is used
fill_on_button = cv2.imread("Images/fill_on_button.png", cv2.IMREAD_GRAYSCALE)
fill_on_button_hovered = cv2.imread("Images/fill_on_button_hovered.png", cv2.IMREAD_GRAYSCALE)
fill_off_button = cv2.imread("Images/fill_off_button.png", cv2.IMREAD_GRAYSCALE)
fill_off_button_hovered = cv2.imread("Images/fill_off_button_hovered.png", cv2.IMREAD_GRAYSCALE)

control_spawn_icon = cv2.imread("Images/control_spawn_icon.png", cv2.IMREAD_GRAYSCALE)
control_spawn_icon_hovered = cv2.imread("Images/control_spawn_icon_hovered.png", cv2.IMREAD_GRAYSCALE)
control_mode_selected = cv2.imread("Images/control_mode_selected.png", cv2.IMREAD_GRAYSCALE)
control_mode_button = cv2.imread("Images/control_mode_button.png", cv2.IMREAD_GRAYSCALE)

gun_run_mode_selected = cv2.imread("Images/gun_run_mode_selected.png", cv2.IMREAD_GRAYSCALE)
gun_run_mode_button = cv2.imread("Images/gun_run_mode_button.png", cv2.IMREAD_GRAYSCALE)

loadout_select_button = cv2.imread("Images/loadout_select_button.png", cv2.IMREAD_GRAYSCALE)

# Optional buttons to auto open apex packs
apex_pack_button = cv2.imread("Images/apex_pack_button.png", cv2.IMREAD_GRAYSCALE)
apex_pack_button_hovered = cv2.imread("Images/apex_pack_button_hovered.png", cv2.IMREAD_GRAYSCALE)
apex_packs_button = cv2.imread("Images/apex_packs_button.png", cv2.IMREAD_GRAYSCALE)
apex_packs_button_hovered = cv2.imread("Images/apex_packs_button_hovered.png", cv2.IMREAD_GRAYSCALE)
apex_pack_remaining_button = cv2.imread("Images/apex_pack_remaining_button.png", cv2.IMREAD_GRAYSCALE)
apex_pack_remaining_button_hovered = cv2.imread("Images/apex_pack_remaining_button_hovered.png", cv2.IMREAD_GRAYSCALE)
apex_packs_remaining_button = cv2.imread("Images/apex_packs_remaining_button.png", cv2.IMREAD_GRAYSCALE)
apex_packs_remaining_button_hovered = cv2.imread("Images/apex_packs_remaining_button_hovered.png", cv2.IMREAD_GRAYSCALE)
zero_apex_packs_remaining_button = cv2.imread("Images/zero_apex_packs_remaining_button.png", cv2.IMREAD_GRAYSCALE)

# Optional buttons to solo drop & launch downwards (in an attempt to die quickly)
relinquish_prompt = cv2.imread("Images/relinquish_prompt.png", cv2.IMREAD_GRAYSCALE)
launch_solo_prompt = cv2.imread("Images/launch_solo_prompt.png", cv2.IMREAD_GRAYSCALE)
launch_prompt = cv2.imread("Images/launch_prompt.png", cv2.IMREAD_GRAYSCALE)


lobby_second_player_missing = cv2.imread("Images/lobby_second_player_missing.png", cv2.IMREAD_GRAYSCALE)
lobby_third_player_missing = cv2.imread("Images/lobby_third_player_missing.png", cv2.IMREAD_GRAYSCALE)


UI_elements = [{'image': return_to_lobby_button, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': return_to_lobby_button_hovered, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': exit_confirm_button, 'threshold': 0.85, 'callback': keyinput.click},
               {'image': exit_confirm_button_hovered, 'threshold': 0.85, 'callback': keyinput.click},
               {'image': leave_button, 'threshold': 0.70, 'callback': keyinput.click},
               {'image': leave_button_hovered, 'threshold': 0.70, 'callback': keyinput.click},
               {'image': ready_button, 'threshold': 0.75, 'callback': ready_up},
               {'image': ready_button_hovered, 'threshold': 0.75, 'callback': ready_up},
               {'image': exit_to_lobby_button, 'threshold': 0.95, 'callback': hold_space},
               {'image': exit_to_lobby_button_hovered, 'threshold': 0.95, 'callback': hold_space},
               {'image': stats_continue_button, 'threshold': 0.75, 'callback': press_space},
               {'image': squad_eliminated_banner, 'threshold': 0.85, 'callback': press_space},
               {'image': solo_eliminated_banner, 'threshold': 0.85, 'callback': press_esc},
               {'image': match_summary_banner, 'threshold': 0.85, 'callback': press_space},
               {'image': champions_banner, 'threshold': 0.85, 'callback': press_space},
               {'image': defeat_banner, 'threshold': 0.85, 'callback': press_space},
               {'image': victory_banner, 'threshold': 0.85, 'callback': press_space},
#               {'image': summary_button, 'threshold': 0.85, 'callback': keyinput.click},
#               {'image': summary_button_hovered, 'threshold': 0.85, 'callback': keyinput.click},
               {'image': dialog_button, 'threshold': 0.85, 'callback': press_esc},
               {'image': news_button, 'threshold': 0.85, 'callback': press_esc},
               {'image': error_dialog_button, 'threshold': 0.85, 'callback': keyinput.click},
               {'image': error_dialog_button_hovered, 'threshold': 0.85, 'callback': keyinput.click},
               {'image': title_screen_continue_button, 'threshold': 0.85, 'callback': keyinput.click},
               {'image': title_screen_retry_button, 'threshold': 0.85, 'callback': keyinput.click},
               {'image': announcement_close_button, 'threshold': 0.85, 'callback': keyinput.click},
               {'image': battle_pass_close_button, 'threshold': 0.85, 'callback': press_esc},
               {'image': battle_pass_close_button_hovered, 'threshold': 0.85, 'callback': press_esc},
               {'image': dev_error_button, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': directx_device_hung, 'threshold': 0.95, 'callback': kill_game_for_error},
               {'image': game_logic_error_message, 'threshold': 0.95, 'callback': kill_game_for_error}]

# Templates of hero icons we are looking for in the character selection screen
heroes = {'ash': cv2.imread("Images/Heroes/ash_icon.png", cv2.IMREAD_GRAYSCALE),
          'ballistic': cv2.imread("Images/Heroes/ballistic_icon.png", cv2.IMREAD_GRAYSCALE),
          'bangalore': cv2.imread("Images/Heroes/bangalore_icon.png", cv2.IMREAD_GRAYSCALE),
          'bloodhound': cv2.imread("Images/Heroes/bloodhound_icon.png", cv2.IMREAD_GRAYSCALE),
          'catalyst': cv2.imread("Images/Heroes/catalyst_icon.png", cv2.IMREAD_GRAYSCALE),
          'caustic': cv2.imread("Images/Heroes/caustic_icon.png", cv2.IMREAD_GRAYSCALE),
          'crypto': cv2.imread("Images/Heroes/crypto_icon.png", cv2.IMREAD_GRAYSCALE),
          'fuse': cv2.imread("Images/Heroes/fuse_icon.png", cv2.IMREAD_GRAYSCALE),
          'gibraltar': cv2.imread("Images/Heroes/gibraltar_icon.png", cv2.IMREAD_GRAYSCALE),
          'horizon': cv2.imread("Images/Heroes/horizon_icon.png", cv2.IMREAD_GRAYSCALE),
          'lifeline': cv2.imread("Images/Heroes/lifeline_icon.png", cv2.IMREAD_GRAYSCALE),
          'loba': cv2.imread("Images/Heroes/loba_icon.png", cv2.IMREAD_GRAYSCALE),
          'maggie': cv2.imread("Images/Heroes/maggie_icon.png", cv2.IMREAD_GRAYSCALE),
          'mirage': cv2.imread("Images/Heroes/mirage_icon.png", cv2.IMREAD_GRAYSCALE),
          'newcastle': cv2.imread("Images/Heroes/mirage_icon.png", cv2.IMREAD_GRAYSCALE),
          'pathfinder': cv2.imread("Images/Heroes/pathfinder_icon.png", cv2.IMREAD_GRAYSCALE),
          'octane': cv2.imread("Images/Heroes/octane_icon.png", cv2.IMREAD_GRAYSCALE),
          'rampart': cv2.imread("Images/Heroes/rampart_icon.png", cv2.IMREAD_GRAYSCALE),
          'revenant': cv2.imread("Images/Heroes/revenant_icon.png", cv2.IMREAD_GRAYSCALE),
          'seer': cv2.imread("Images/Heroes/seer_icon.png", cv2.IMREAD_GRAYSCALE),
          'valkyrie': cv2.imread("Images/Heroes/valkyrie_icon.png", cv2.IMREAD_GRAYSCALE),
          'vantage': cv2.imread("Images/Heroes/vantage_icon.png", cv2.IMREAD_GRAYSCALE),
          'wattson': cv2.imread("Images/Heroes/wattson_icon.png", cv2.IMREAD_GRAYSCALE),
          'wraith': cv2.imread("Images/Heroes/wraith_icon.png", cv2.IMREAD_GRAYSCALE)}

# Parse launch options if any
if len(sys.argv) > 1:
    # Regex used to parse options
    arg_regex_template = '(.*)=(.*)'
    arg_regex = re.compile(arg_regex_template)

    for argv in sys.argv[1:]:
        match = arg_regex.match(argv)
        if match is not None:
            launch_options[match.group(1)] = match.group(2)
        else:
            launch_options[argv] = None

# Main procedure really starts here
# Add character selection when provided by the user
if 'hero' in launch_options and launch_options['hero'] in heroes:
    log("Selected hero: " + launch_options['hero'])
    # Append the character button to the UI elements to click on
    UI_elements.append({'image': heroes[launch_options['hero']], 'threshold': 0.85, 'callback': keyinput.click})

# Notify the bot it has to leave the game if respawn expired
if 'leave-on-respawn-expired' in launch_options:
    log('Will leave when respawn expires')
    UI_elements.insert(0, {'image': open_menu_button, 'threshold': 0.95, 'callback': press_esc})

    # We prepend the 'you can still be respawned' icon to leave the menu
    # This should keep opening and closing the menu until it disappears, ie respawn expired
    UI_elements.insert(0, {'image': leave_match_can_be_respawned, 'threshold': 0.95, 'callback': press_esc})

# Launch solo and make sure to aim down
if 'launch-solo-and-die' in launch_options:
    log("Will launch solo and die")
    UI_elements.append({'image': relinquish_prompt, 'threshold': 0.85, 'callback': hold_ctrl})
    UI_elements.append({'image': launch_solo_prompt, 'threshold': 0.85, 'callback': hold_ctrl})
    UI_elements.append({'image': launch_prompt, 'threshold': 0.85, 'callback': press_e_and_aim_down})


# Launch solo if possible and randomly choose when to drop
if 'random-launch' in launch_options:
    log("Will launch at a random time and disjoint when possible")
    UI_elements.append({'image': launch_solo_prompt, 'threshold': 0.85, 'callback': hold_ctrl})
    UI_elements.append({'image': launch_prompt, 'threshold': 0.85, 'callback': launch_at_random_time})


# Fill or no-fill
if 'fill' in launch_options:
    if launch_options['fill'] == 'on':
        log('Fill: ON')
        UI_elements.insert(0, {'image': fill_off_button, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': fill_off_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})
    elif launch_options['fill'] == 'off':
        log('Fill: OFF')
        UI_elements.insert(0, {'image': fill_on_button, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': fill_on_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})
    else:
        log(f"Unknown fill option: {launch_options['fill']}")

if 'steam' in launch_options:
    log('Steam version specified')
    isSteamVersion = True

if 'mode' in launch_options:
    UI_elements.insert(0, {'image': training_mode_selected, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.insert(0, {'image': firing_range_mode_selected, 'threshold': 0.95, 'callback': keyinput.click})

    if launch_options['mode'] == 'ranked':
        log('Selected mode: Ranked')
        UI_elements.insert(0, {'image': control_mode_selected, 'threshold': 0.90, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': trios_mode_selected, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': duos_mode_selected, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': tdm_mode_selected, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': gun_run_mode_selected, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': ranked_mode_button, 'threshold': 0.95, 'callback': keyinput.click})

    if launch_options['mode'] == 'trios':
        log('Selected mode: Trios')
        UI_elements.insert(0, {'image': control_mode_selected, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': ranked_mode_selected, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': duos_mode_selected, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': tdm_mode_selected, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': gun_run_mode_selected, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': trios_mode_button, 'threshold': 0.95, 'callback': keyinput.click})

    if launch_options['mode'] == 'duos':
        log('Selected mode: Duos')
        UI_elements.insert(0, {'image': control_mode_selected, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': trios_mode_selected, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': ranked_mode_selected, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': tdm_mode_selected, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': gun_run_mode_selected, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': duos_mode_button, 'threshold': 0.95, 'callback': keyinput.click})

    if launch_options['mode'] == 'mixtape':
        log('Selected modes: Mixtape')
        UI_elements.insert(0, {'image': trios_mode_selected, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': duos_mode_selected, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': ranked_mode_selected, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': tdm_mode_button, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': control_mode_button, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': gun_run_mode_button, 'threshold': 0.85, 'callback': keyinput.click})

        UI_elements.insert(0, {'image': control_spawn_icon, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': control_spawn_icon_hovered, 'threshold': 0.85, 'callback': keyinput.click})
        UI_elements.insert(0, {'image': loadout_select_button, 'threshold': 0.95, 'callback': keyinput.click})


if 'open-packs' in launch_options:
    log('Will automatically open packs')
    UI_elements.insert(0, {'image': apex_pack_button, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.insert(0, {'image': apex_pack_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.insert(0, {'image': apex_packs_button, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.insert(0, {'image': apex_packs_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.insert(0, {'image': apex_pack_remaining_button, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.insert(0, {'image': apex_pack_remaining_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.insert(0, {'image': apex_packs_remaining_button, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.insert(0, {'image': apex_packs_remaining_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.insert(0, {'image': zero_apex_packs_remaining_button, 'threshold': 0.95, 'callback': press_esc})


if 'no-freeze-watchdog' in launch_options:
    log('Will not kill the game if it appears frozen')
    kill_if_frozen = False


if 'no-ready-timeout' in launch_options:
    log("Will not kill the game if we haven't been readying up for too long")
    kill_if_ready_timeout = False


if 'lobby-size' in launch_options:
    log('Will wait for the lobby to be full to ready-up')
    lobby_size = int(launch_options['lobby-size'])
    if lobby_size >= 3:
        UI_elements.insert(0, {'image': lobby_third_player_missing, 'threshold': 0.90, 'callback': wait_for_third_player})
    if lobby_size >= 2:
        UI_elements.insert(0, {'image': lobby_second_player_missing, 'threshold': 0.90, 'callback': wait_for_second_player})


# Set ourselves as DPI aware, or else we won't get proper pixel coordinates if scaling is not 100%
errorCode = ctypes.windll.shcore.SetProcessDpiAwareness(2)

log("Apex bot starting in 5 seconds, bring apex window in focus...")
time.sleep(5)

win32gui.EnumWindows(getapexwindow, None)

# If the window hasn't been found, exit
if not window_found:
    log("No Apex Legends window found")
    sys.exit(1)

# Start the watchdog thread
watchdogthread = threading.Thread(target=process_watchdog)
watchdogthread.start()

while True:
    # If the window died, stop running analysis for a moment */
    if window_found is False:
        time.sleep(5)
        continue

    # Capture the window in color
    color_img = screenshot(window_x, window_y, window_width, window_height)
    # Convert it to grayscale for faster processing
    img = cv2.cvtColor(color_img, cv2.COLOR_BGR2GRAY)

    found_UI_element = False

    # Try to find any of the templates
    for element in UI_elements:
        result = find_item(img, element['image'])

        if result['threshold'] > element['threshold']:
            found_UI_element = True

            # Click all elements that potentially match
            if 'multiple' in element:
                num_of_points = int(element['multiple'])
                raw_result = result['raw_result']

                # Find the highest matching patterns
                coords_flattened = np.argpartition(raw_result.flatten(), -num_of_points)[-num_of_points:]
                coords_2d_yx = np.unravel_index(coords_flattened, raw_result.shape)

                coords = []

                w, h = element['image'].shape[::-1]

                for y, x in zip(coords_2d_yx[0], coords_2d_yx[1]):
                    coords.append((x + w / 2, y + h / 2))

                # Click them all with a delay of half a second inbetween
                for coord in coords:
                    element['callback'](window_x + int(coord[0]), window_y + int(coord[1]))
                    time.sleep(0.5)
            else:
                element['callback'](window_x + int(result['x']), window_y + int(result['y']))
            break

    # If no UI element got found, move around is the default behavior
    if found_UI_element is False:
        move_around()

    # Sleep for a second
    time.sleep(1.0)

    if kill_if_ready_timeout:
        # In case we haven't been readying up for an hour, kill the game
        current_time = time.time()

        if current_time - last_ready_up > 3600:
            kill_game_for_ready_timeout()
